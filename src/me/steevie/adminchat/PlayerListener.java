package me.steevie.adminchat;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener{
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		Player player = event.getPlayer();
		
		if (PlayerManager.DEFAULT_ACTIVE){
			if (player.isOp() || player.hasPermission("adminchat.receive")){
				AdminChat.getPlayerManager().toggleAdminChat(player, true);
			}
		}
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event){
		Player player = event.getPlayer();
		
		String message = event.getMessage();
		String PREFIX = AdminChat.getPrefix();
		
		char trigger = (char) AdminChat.getInstance().getConfig().getString("chat_sender_character").chars().toArray()[0];
		
		if (player.hasPermission("adminchat.send")){
			
			if (AdminChat.getPlayerManager().isToggled(player)){
				// loop thru players & cancel event
				event.setCancelled(true);
				for (Player pl : Bukkit.getOnlinePlayers()){
					// send message to player if its OP or has permission
					if (pl.hasPermission("adminchat.receive") || pl.isOp()){
						if (AdminChat.getPlayerManager().isToggled(player)){
							if (pl.equals(player)){
								continue;
							}
							pl.sendMessage(AdminChat.color(PREFIX + player.getName() + ": " + message));
						}
					}
				}
				// finally send message to sender	
				player.sendMessage(AdminChat.color(PREFIX + player.getName() + ": " + StringUtils.remove(message, trigger)));
			}
			
			else if (message.startsWith("" + trigger)){
				// loop thru players & cancel event
				event.setCancelled(true);
				for (Player pl : Bukkit.getOnlinePlayers()){
					// send message to player if its OP or has permission
					if (pl.hasPermission("adminchat.receive") || pl.isOp()){
						if (AdminChat.getPlayerManager().isToggled(player)){
							if (pl.equals(player)){
								continue;
							}
							pl.sendMessage(AdminChat.color(PREFIX + player.getName() + ": " + StringUtils.remove(message, trigger)));
						}
					}
				}
				// finally send message to sender	
				player.sendMessage(AdminChat.color(PREFIX + player.getName() + ": " + StringUtils.remove(message, trigger)));
			}
		}
	}

}
