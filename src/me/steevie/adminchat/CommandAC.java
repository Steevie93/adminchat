package me.steevie.adminchat;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class CommandAC implements CommandExecutor{
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (!(sender instanceof Player)){
			sender.sendMessage(ChatColor.RED + "You must be an in-game player to do this!");
			return false;
		}
		
		Player player = (Player) sender;
		
		// player didn't type arguments (or subcommands)
		if (args.length == 0){
			player.sendMessage(ChatColor.RED + "Usage:");
			player.sendMessage(ChatColor.RED + "/adminchat toggle");
			return true;
		}
		
		else if (args.length == 1){
			if (args[0].equalsIgnoreCase("toggle")){
				if (player.hasPermission("adminchat.toggle")){
					
					// player has adminchat on, so turn off
					if (AdminChat.getPlayerManager().isToggled(player)){
						AdminChat.getPlayerManager().toggleAdminChat(player, false);
						player.sendMessage("AdminChat status: false");
						return true;
					}
					
					// player has adminchat off, so turn on
					else {
						AdminChat.getPlayerManager().toggleAdminChat(player, true);
						player.sendMessage("AdminChat status: true");
						return true;
					}
					
				} else {
					player.sendMessage(ChatColor.RED + "No permission!");
					return false;
				}
			}
		}
		
		return false;
	}

}
