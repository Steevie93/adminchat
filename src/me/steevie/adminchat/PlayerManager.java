package me.steevie.adminchat;

import org.bukkit.entity.Player;

public class PlayerManager {
	
	private AdminChat plugin;
	
	public static boolean DEFAULT_ACTIVE;
	
	// constructor
	public PlayerManager(AdminChat plugin){
		this.plugin = plugin;
		
		DEFAULT_ACTIVE = plugin.getConfig().getBoolean("ActiveByDefault");
	}
	
	public void toggleAdminChat(Player player, boolean b){
		plugin.getConfig().set("player-settings." + player.getUniqueId() + ".enabled", b);
		plugin.saveConfig();
	}
	
	public boolean isToggled(Player player){
		if (!plugin.getConfig().contains("player-settings." + player.getUniqueId() + ".enabled")){
			return false;
		}
		return plugin.getConfig().getBoolean("player-settings." + player.getUniqueId() + ".enabled");
	}

}
