package me.steevie.adminchat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.steevie.adminchat.PlayerManager;

public class AdminChat extends JavaPlugin implements Listener{
	
	private static AdminChat instance;
	private static String PREFIX;
	
	private static PlayerManager manager;
	
	public void onEnable(){
		// init instance
		instance = this;
		
		// init PlayerManager 
		manager = new PlayerManager(this);

		// load config
		getConfig().options().copyDefaults(true);
		saveConfig();
		
		// init PREFIX & color it
		PREFIX = color(getConfig().getString("prefix"));
		
		// register events
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new PlayerListener(), this);
		
		// register commands
		getCommand("adminchat").setExecutor(new CommandAC());
	}
	
	/**
	 * Returns prefix in config
	 * return PREFIX
	 */
	public static String getPrefix(){
		return PREFIX;
	}
	
	/**
	 * Add colors to String based on "&" color codes
	 * @param string
	 * @return string
	 */
	public static String color(String string){
		string = ChatColor.translateAlternateColorCodes('&', string);
		return string;
	}
	
	/**
	 * Returns instance of plugin
	 * @return
	 */
	public static AdminChat getInstance(){
		return instance;
	}

	/**
	 * Returns instance of PlayerManager
	 * @return
	 */
	public static PlayerManager getPlayerManager(){
		return manager;
	}

}
